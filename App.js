/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import reduxStore from './src/redux/store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Routes from './src/routes';

this.reduxStore = null;

export default class App extends Component {

  componentWillMount() {
    /**
     * redux used to define global level state of the application like 
     * languages, session token and eth
     */
    this.reduxStore = reduxStore();
  }

  render() {
    return (
      <Provider store={this.reduxStore.store}>
        <PersistGate loading={null} persistor={this.reduxStore.persistor}>
          <View style={{ flex: 1 }}>
            <Routes />
          </View>
        </PersistGate>
      </Provider>
    );
  }
}
