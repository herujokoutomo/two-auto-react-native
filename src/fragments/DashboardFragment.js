import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux';
import {mapStateToProps, mapDispatchToProps} from '../helpers/reduxMapper';
import LocalizedStrings from 'react-localization';
import enDictionary from '../languages/en.json';
import idDictionary from '../languages/id.json';

let strings = new LocalizedStrings({
    EN: enDictionary,
    ID: idDictionary
});

class DashboardFragment extends Component {

    constructor() {
        super();
    }

    _setupLanguage() {
        strings.setLanguage(this.props.app.language.toUpperCase());
    }

    componentWillMount() {
        this._setupLanguage();
    }

    render() {
        return (
            <View style={{ flex: 1 , justifyContent: 'center', alignItems: 'center'}}>
                <Text>{strings.DASHBOARD}</Text>
                <Text style={{ textAlign: 'center'}}>{
                    JSON.stringify(this.props)
                }</Text>
            </View>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (DashboardFragment);