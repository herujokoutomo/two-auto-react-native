import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../helpers/reduxMapper';
import { MAIN_APP_COLOR } from '../../globals';

import LocalizedStrings from 'react-localization';
import enDictionary from '../languages/en.json';
import idDictionary from '../languages/id.json';

let strings = new LocalizedStrings({
    EN: enDictionary,
    ID: idDictionary
});
class ProfileFragment extends Component {

    constructor() {
        super();

        this._changeLanguage = this.changeLanguage.bind(this);
        this._navigateToAbout = this.navigateToAbout.bind(this);
    }

    _setupLanguage() {
        strings.setLanguage(this.props.app.language.toUpperCase());
    }

    changeLanguage() {
        const self = this;
        let language = 'id';
        if (this.props.app.language === 'id') {
            language = 'en';
        }

        strings.setLanguage(language.toUpperCase());    
        setTimeout(() => {
            self.props.actions.setLanguage(language);    
        }, 50);
    }

    navigateToAbout() {
        const {navigate} = this.props.navigation;
        navigate('About');
    }

    componentWillMount() {
        this._setupLanguage();
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>{strings.PROFILE}</Text>
                <Text style={{ textAlign: 'center' }}>{
                    JSON.stringify(this.props)
                }</Text>

                <TouchableOpacity onPress={() => this._changeLanguage()} style={{ marginTop: 10, backgroundColor: MAIN_APP_COLOR, padding: 10 }}>
                    <Text style={{ color: '#FFF' }}>{strings.CHANGE_LANGUAGE}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this._navigateToAbout()} style={{ marginTop: 10, backgroundColor: MAIN_APP_COLOR, padding: 10 }}>
                    <Text style={{ color: '#FFF' }}>{strings.ABOUT}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileFragment);