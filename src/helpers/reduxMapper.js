import { bindActionCreators } from 'redux';
import * as authActions from '../redux/actions/authActions';
import * as appActions from '../redux/actions/appActions';

export function mapStateToProps(state) {
    return state;
}

export function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Object.assign({}, authActions, appActions), dispatch)
    };
}
