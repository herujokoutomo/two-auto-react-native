import api from './apiService';

const submitLogin = (username, password) => {
    return api.post('login', { username, password });
}

export default {
    submitLogin
}
