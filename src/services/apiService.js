import axios from 'axios';
import { AsyncStorage } from 'react-native';

const instance = axios.create({
    baseURL: 'https://some-domain.com/api/',
    timeout: 30000,
});

instance.interceptors.request.use(async function (config) {
    // Do something before request is sent
    let token = await AsyncStorage.getItem('token');
    let headers = { 'Authorization': token };

    console.log('REQUEST HEADER', headers);

    config.headers = Object.assign({} , config.headers, headers);
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

export default instance;