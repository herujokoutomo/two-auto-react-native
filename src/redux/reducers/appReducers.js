export default function (state = { language: 'id' }, action) {
    switch (action.type) {
        case 'SET_LANGUAGE':
            state.language = action.language;
            return Object.assign({}, state)
        default:
            return state;
    }
}