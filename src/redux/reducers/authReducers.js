export default function (state = { session: null }, action) {
    switch (action.type) {
        case 'SET_SESSION':
            state.session = action.payload;
            return Object.assign({}, state)
        case 'DESTROY_SESSION':
            state.session = null;
            return state;
        default:
            return state;
    }
}