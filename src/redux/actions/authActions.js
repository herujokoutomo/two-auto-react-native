export function setSession(payload) {
    return {
        type: "SET_SESSION",
        payload: payload
    }
}

export function destorySession() {
    return {
        type: "DESTROY_SESSION"
    }
}