
import { createStore, combineReducers, compose } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { AsyncStorage } from 'react-native'
import authReducers from './reducers/authReducers'
import appReducers from './reducers/appReducers'

const persistConfig = {
    key: 'root',
    storage
}

let combinedReducers = combineReducers({
    session: authReducers,
    app: appReducers
});

const persistedReducer = persistReducer(persistConfig, combinedReducers);

export default () => {
    let store = createStore(persistedReducer)
    let persistor = persistStore(store)
    return { store, persistor }
}