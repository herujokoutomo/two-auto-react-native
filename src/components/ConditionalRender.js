import React, { Component } from 'react';

class ConditionalRender extends Component {

    render() {
        return this.props.when? this.props.children : null;
    }

}

export default ConditionalRender;