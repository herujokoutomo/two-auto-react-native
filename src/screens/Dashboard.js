import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import { MAIN_APP_COLOR, INACTIVE_COLOR, APP_NAME } from '../../globals';
import ConditionalRender from '../components/ConditionalRender';


// fragements
import DashboardFragment from '../fragments/DashboardFragment';
import ProfileFragment from '../fragments/ProfileFragment';

class Dashboard extends Component {

    constructor() {
        super();
        this.state = {
            fragment: 'dashboard'
        };

        this._changeFragment = this.changeFragment.bind(this);
    }

    static navigationOptions = {
        // headerTitle instead of title
        headerTitle: <Text style={{ color: '#fff', fontSize: 20, marginLeft: 10}}>2 Auto</Text>,
    };

    changeFragment(fragment) {
        this.setState({ fragment });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {/* <Header
                    centerComponent={{ text: APP_NAME, style: { color: '#fff' } }}
                    rightComponent={
                        <Icon
                            name='logout'
                            type='material-community'
                            color='#fff'
                        />
                    }
                    backgroundColor={MAIN_APP_COLOR}
                /> */}
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 10 }}>
                        <ConditionalRender when={this.state.fragment === 'dashboard'}>
                            <DashboardFragment navigation={this.props.navigation}/>
                        </ConditionalRender>
                        <ConditionalRender when={this.state.fragment === 'profile'}>
                            <ProfileFragment navigation={this.props.navigation}/>
                        </ConditionalRender>
                    </View>
                    <View style={{ backgroundColor: "#eee", height: 20, flex: 1, flexDirection: 'row', justifyContent: 'space-around', }}>
                        <TouchableOpacity onPress={() => this._changeFragment('orders')} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon
                                color={this.state.fragment === 'orders' ? '#000' : INACTIVE_COLOR}
                                type='material-community'
                                name='file' />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this._changeFragment('dashboard')} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon
                                color={this.state.fragment === 'dashboard' ? '#000' : INACTIVE_COLOR}
                                name='dashboard' />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this._changeFragment('profile')} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon
                                color={this.state.fragment === 'profile' ? '#000' : INACTIVE_COLOR}
                                name='person' />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

export default Dashboard;
