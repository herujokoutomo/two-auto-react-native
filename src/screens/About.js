import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ToastAndroid } from 'react-native';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from '../helpers/reduxMapper';
import { MAIN_APP_COLOR, INACTIVE_COLOR } from '../../globals';

import LocalizedStrings from 'react-localization';
import enDictionary from '../languages/en.json';
import idDictionary from '../languages/id.json';

import authService from '../services/authService';

let strings = new LocalizedStrings({
    EN: enDictionary,
    ID: idDictionary
});

class About extends Component {

    constructor() {
        super();
        this.state = {
            sending: false
        }

        this._doLogin = this.doLogin.bind(this);
    }

    static navigationOptions = {
        // headerTitle instead of title
        headerTitle: <Text style={{ color: '#fff', fontSize: 20, marginLeft: 10}}>2 Auto</Text>,
    };

    async doLogin() {
        try {
            this.setState({sending: true});
            let response = await authService.submitLogin('test@gmail.com', '123456');
            console.log(response.data);
            this.setState({sending: false});
            ToastAndroid.show(JSON.stringify(response.data), ToastAndroid.SHORT);
        } catch(err) {
            this.setState({sending: false});
            console.log(err);
            ToastAndroid.show(JSON.stringify(err), ToastAndroid.SHORT);
        }
    }

    _setupLanguage() {
        strings.setLanguage(this.props.app.language.toUpperCase());
    }

    componentWillMount() {
        this._setupLanguage();
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text>{strings.ABOUT}</Text>
                <TouchableOpacity disabled={this.state.sending} onPress={() => this._doLogin()} style={{ marginTop: 10, backgroundColor: this.state.sending? INACTIVE_COLOR : MAIN_APP_COLOR, padding: 10 }}>
                    <Text style={{ color: '#FFF' }}>
                        {
                            this.state.sending? 'Requesting...' : 'Test network request'
                        }
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(About);