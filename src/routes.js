import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation';
import { MAIN_APP_COLOR, INACTIVE_COLOR, APP_NAME } from '../globals';

import DashboardScreen from './screens/Dashboard';
import AboutScreen from './screens/About';

const navigator = createStackNavigator({
    Dashboard: {
        screen: DashboardScreen
    },
    About: {
        screen: AboutScreen
    }
}, {
        defaultNavigationOptions: {
            headerTintColor: '#fff',
            headerStyle: {
                backgroundColor: MAIN_APP_COLOR,
            },
        },
        navigationOptions: {
            tabBarLabel: 'Home!',
        },
    });

export default createAppContainer(navigator);