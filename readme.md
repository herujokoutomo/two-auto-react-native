# Getting Started

#### Install React Native

To install react native please follow guide here [https://facebook.github.io/react-native/docs/getting-started.html](https://facebook.github.io/react-native/docs/getting-started.html)

#### Install Project depedency

`npm install` or `yarn`

#### Run the app

Run your favorite android emulator (genymotion or from android studio) then run this command `npm run start` and another tab of terminal in same project run also `react-native run-android`

#### Log the output

To log the output of the app can simply run `react-native log-android` in another terminal tab or use browser integrated described here [https://facebook.github.io/react-native/docs/debugging](https://facebook.github.io/react-native/docs/debugging)